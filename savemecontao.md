#Workflows
##Paten
Neue Paten werden per E-Mail gemeldet. Angemeldete Paten müssen über das Contao-CMS freigeschaltet werden, sonst sieht man sie nicht. Über den Menüpunkt *Paten* auf der linken Seite erreicht man die Verwaltung der Paten.

Dort kann man über die Suche verschiedene Filter einstellen. Diese sind meist selbsterklärend. Neu angemeldete aber nicht aktivierte Paten sieht man mittels der Einstellung `aktiviert=0`.

##Patenfotos
Wenn Paten ein Foto hochgeladen haben, so erscheint dies als Anhang in der E-Mail. Das Foto muss per Hand hochgeladen werden.
##Termine
##Fotoalben
##Neuigkeiten
##Presse

#Tools
##ImageMagick

~~~bash
#!/bin/bash
mkdir x800
mkdir x1024
for IMG in *{jpg,JPG}
do
    convert $IMG -scale 800 -compress JPEG -quality 75 x800/$IMG
    convert $IMG -scale 1024 -compress JPEG -quality 75 x1024/$IMG
    mogrify -compress JPEG -quality 75 $IMG
done
~~~
##GIMP

#Best Practice
##Batch-Renaming